# Ansible Role: Borgmatic (BorgBackup) Client

Set up encrypted, compressed and deduplicated backups using [BorgBackup](https://borgbackup.readthedocs.io/en/stable/) and [Borgmatic](https://github.com/witten/borgmatic). Currently supports Debian/Ubuntu and CentOS/Red Hat, ArchLinux.


## Example Playbooks

### normal, push-based backup to two backup servers

```
- hosts: desktops
  become: true
  tasks:
    - name: borgmatic setup
      include_role: 
        name: networkjanitor.borgmatic
      vars:
        borgmatic_encryption_passphrase: CHANGEME
        borgmatic_backupservers: 
          - backuphost01
          - backuphost02
        borgmatic_create_repo: True
        borgmatic_source_directories:
          - /home/networkjanitor/
        borgmatic_retention_policy:
          keep_hourly: 3
          keep_daily: 7
          keep_weekly: 4
          keep_monthly: 6

```

### reverse-tunnelled, push-based backup to two backup servers

```
- hosts: vps
  become: true
  tasks:
    - name: borgmatic setup
      include_role: 
        name: networkjanitor.borgmatic
      vars:
        borgmatic_encryption_passphrase: CHANGEME
        borgmatic_backupservers: 
          - backuphost01
          - backuphost02
        borgmatic_reverse_tunnel_enable: True
        # In reverse tunnel mode, for every backupserver listed in borgmatic_backupservers
        # one unique port has to be configured here.
        borgmatic_servertobackup_reverse_tunnel_ssh_ports:
          - 22222
          - 22223
        borgmatic_create_repo: True
        borgmatic_source_directories:
          - /data
        borgmatic_retention_policy:
          keep_hourly: 3
          keep_daily: 7
          keep_weekly: 4
          keep_monthly: 6

```


## Installation

Clone to local folder

```
$ git clone https://gitlab.com/networkjanitor/ansible-role-borgmatic.git roles/networkjanitor.borgmatic
```


## Role Variables

### Required Variables
- `borgmatic_backupservers`: The servers on which the backups are saved.
- `borg_encryption_passphrase`: Password to use for repokey.

For a list of all optional variables including explanations, see `defaults/main.yml`.

## Important Notes

- this role does not create the users defined in `borgmatic_backupserver_user` and `borgmatic_servertobackup_user`, they have to exist beforehand.

## License

MIT

Initially based on https://github.com/borgbase/ansible-role-borgbackup but largely rewritten for systemd, reverse tunneling capabilities and more. 

